use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::Mutex;
use url::Url;

pub struct UrlMap {
    inner: Arc<Mutex<HashMap<Url, UrlMapValue>>>,
}

impl UrlMap {
    /// Get a given URL's status, setting it to pending if it doesn't exist yet
    pub async fn get_status_or_init(&self, url: &Url) -> Option<UrlMapValue> {
        let mut map = self.inner.lock().await;
        let status = map.get(&url).map(|x| x.clone());
        if status.is_some() {
            return status;
        } else {
            map.insert(url.clone(), UrlMapValue::Pending);
            return status;
        }
    }

    /// Set a given URL's status to unusable with a given error message
    pub async fn set_unusable(&self, url: &Url, message: String) {
        let mut map = self.inner.lock().await;
        map.insert(url.clone(), UrlMapValue::Unusable(message));
    }

    /// Set a given URL's status to exists with a given shortname
    pub async fn set_exists(&self, url: &Url, shortname: String) {
        let mut map = self.inner.lock().await;
        map.insert(url.clone(), UrlMapValue::Exists(shortname));
    }
}

impl Default for UrlMap {
    fn default() -> Self {
        Self {
            inner: Arc::new(Mutex::new(HashMap::new())),
        }
    }
}

/// Addition status of a URL
#[derive(Clone, Debug)]
pub enum UrlMapValue {
    /// Channel with this URL is being added
    Pending,

    /// Channel with this URL exists
    Exists(String),

    /// This URL failed previously
    Unusable(String),
}

pub struct UpdateLock {
    inner: Arc<Mutex<InnerUpdateLock>>,
}

impl UpdateLock {
    /// Attempt to lock a specific channel for updating
    pub async fn request_one(&self, shortname: &String) -> RequestLockResult {
        let mut inner = self.inner.lock().await;

        // If locked, deny
        if inner.total_lock {
            return RequestLockResult::DeniedByTotal;
        }
        if inner.individual_locks.get(shortname) == Some(&true) {
            return RequestLockResult::DeniedByPartial;
        }

        // If unlocked, lock and grant
        inner.individual_count += 1;
        inner.individual_locks.insert(shortname.clone(), true);
        RequestLockResult::Granted
    }

    /// Attempt to lock all channels for updating
    pub async fn request_all(&self) -> RequestLockResult {
        let mut inner = self.inner.lock().await;

        // If locked, deny
        if inner.total_lock {
            return RequestLockResult::DeniedByTotal;
        }
        if inner.individual_count > 0 {
            return RequestLockResult::DeniedByPartial;
        }

        // If unlocked, lock and grant
        inner.total_lock = true;
        RequestLockResult::Granted
    }

    /// Release lock on a specific channel
    pub async fn release_one(&self, shortname: &String) {
        let mut inner = self.inner.lock().await;

        // Panic upon any inconsistencies, unlock otherwise
        if inner.total_lock {
            unreachable!("release_one called on {shortname} but total lock was in effect");
        }
        match inner.individual_locks.insert(shortname.clone(), false) {
            None => unreachable!("release_one called on {shortname} but it was never locked"),
            Some(false) => unreachable!("release_one called on {shortname} but it was not locked"),
            Some(true) => inner.individual_count -= 1,
        }
    }

    /// Release lock on all channels
    pub async fn release_all(&self) {
        let mut inner = self.inner.lock().await;

        // Panic upon any inconsistencies, unlock otherwise
        if inner.individual_count != 0 {
            let n = inner.individual_count;
            unreachable!("release_all called but {n} individual channels were locked");
        }
        if !inner.total_lock {
            unreachable!("release_all called but total lock was not in effect");
        }
        inner.total_lock = false;
    }
}

impl Default for UpdateLock {
    fn default() -> Self {
        Self {
            inner: Arc::new(Mutex::new(InnerUpdateLock::default())),
        }
    }
}

struct InnerUpdateLock {
    individual_locks: HashMap<String, bool>,
    individual_count: usize,
    total_lock: bool,
}

impl Default for InnerUpdateLock {
    fn default() -> Self {
        Self {
            individual_locks: HashMap::new(),
            individual_count: 0,
            total_lock: false,
        }
    }
}

pub enum RequestLockResult {
    /// The lock has been granted
    Granted,

    /// The lock could not be granted because this channel (in a request_one) or any channel (in a
    /// request_all) is already locked
    DeniedByPartial,

    /// The lock could not be granted because every channel is already locked
    DeniedByTotal,
}
