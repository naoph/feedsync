use diesel::prelude::*;
use rss::Channel;
use url::Url;

use feedsync::{models::InsChannel, schema::channels};

use crate::{CLIENT, URLMAP, POOL};

pub async fn new_channel(url: Url) {
    // Attempt to get the channel, otherwise mark the URL unusable
    let channel = match CLIENT.get_channel(url.clone()).await {
        Ok(c) => c,
        Err(e) => {
            URLMAP.set_unusable(&url, e.to_string()).await;
            info!("Add channel failed: {e}");
            return;
        }
    };

    // Add to the database
    let ins = make_ins_channel(channel.clone(), url.clone()).await;
    let conn = match POOL.get() {
        Ok(c) => c,
        Err(e) => {
            error!("Couldn't get database connection: {e}");
            URLMAP.set_unusable(&url, e.to_string()).await;
            return;
        }
    };
    let result: Result<usize, _> = diesel::insert_into(channels::table)
        .values(&ins)
        .execute(&conn);

    // Mark the URL as existing if successful
    match result {
        Ok(_) => {
            URLMAP.set_exists(&url, ins.shortname.clone()).await;
            info!("Add channel succeeded: {}", ins.shortname);
        },
        Err(e) => {
            URLMAP.set_unusable(&url, e.to_string()).await;
            error!("Add channel failed: {}", ins.shortname);
        },
    }
}

async fn make_ins_channel(channel: Channel, url: Url) -> InsChannel {
    // Get author, subtitle, and some categories from itunes extension if it exists
    let itunes = channel.itunes_ext;
    let (d_author, d_subtitle, it_cats) = match itunes {
        None => (None, None, Vec::new()),
        Some(i) => (i.author, i.subtitle, i.categories),
    };

    // Get categories
    let mut cats: Vec<String> = channel.categories.into_iter()
        .map(|c| c.name)
        .collect();
    for it_cat in it_cats {
        let cat = it_cat.text;
        cats.push(cat);
    }
    cats.sort();
    cats.dedup();

    // Generate shortname from title
    // TODO: handle duplicates
    let shortname: String = channel.title.clone()
        .chars()
        .filter(|c| c.is_ascii_alphanumeric())
        .map(|c| c.to_ascii_lowercase())
        .collect();

    let ins = InsChannel {
        remote: url.to_string(),
        shortname,
        active: true,

        d_title: channel.title,
        d_link: channel.link,
        d_description: channel.description,
        d_categories: cats,
        d_image: channel.image.map(|i| i.url),
        d_author,
        d_subtitle,

        c_image: None,
    };

    ins
}
