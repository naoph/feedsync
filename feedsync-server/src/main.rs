#[macro_use] extern crate lazy_static;
#[macro_use] extern crate log;

mod rssproc;
mod remote;
mod routes;
mod state;

use std::error::Error;

use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};

type PgPool = r2d2::Pool<ConnectionManager<PgConnection>>;

lazy_static! {
    pub static ref CLIENT: remote::WebClient = remote::WebClient::new();
    pub static ref URLMAP: state::UrlMap = state::UrlMap::default();
    pub static ref UPDATELOCK: state::UpdateLock = state::UpdateLock::default();

    pub static ref POOL: PgPool = {
        match setup_database() {
            Ok(p) => p,
            Err(e) => {
                error!("Failed to establish database connection: {e}");
                std::process::exit(1);
            },
        }
    };
}

/// Create database connection pool
fn setup_database() -> Result<PgPool, Box<dyn Error>> {
    let url = std::env::var("DATABASE_URL")?;
    let manager = ConnectionManager::<PgConnection>::new(url);
    let pool = r2d2::Pool::builder().build(manager)?;
    Ok(pool)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();
    dotenv::dotenv().unwrap();
    debug!("{} {}", env!("CARGO_PKG_NAME"), env!("CARGO_PKG_VERSION"));

    // Explicitly use the database pool so any errors appear immediately
    POOL.get().unwrap();

    // Start server
    let routes = routes::routes().await;
    Ok(warp::serve(routes).run(([127, 0, 0, 1], 8080)).await)
}
