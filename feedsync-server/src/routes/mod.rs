use warp::{Filter, filters::BoxedFilter, Reply};

mod endpoints;

/// Assemble all filters into one
pub async fn routes() -> BoxedFilter<(impl Reply,)> {
    let routes = warp::any()
        .and(warp::path("v0")
             .and(api_v0().await));

    routes.boxed()
}

/// Assemble API v0 endpoints into one filter to be mounted at /v0
pub async fn api_v0() -> BoxedFilter<(impl Reply,)> {
    let get_version = warp::get()
        .and(warp::path("version"))
        .and_then(endpoints::version);
    let post_channel_add = warp::post()
        .and(warp::path!("channel" / "add"))
        .and(warp::body::json())
        .and_then(endpoints::channel_add);

    let joined = warp::any()
        .and(get_version
             .or(post_channel_add));

    joined.boxed()
}
