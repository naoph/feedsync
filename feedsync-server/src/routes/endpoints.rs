use warp::{reply::Json, Rejection};

use feedsync::api::request::*;
use feedsync::api::response::*;

use crate::URLMAP;
use crate::rssproc;

/// GET /vx/version
pub async fn version() -> Result<Json, Rejection> {
    let version = env!("CARGO_PKG_VERSION");
    let response = feedsync::api::response::VersionResp::from(version);
    Ok(warp::reply::json(&response))
}

/// POST /vx/channel/add
pub async fn channel_add(request: AddChannelReq) -> Result<Json, Rejection> {
    use crate::state::UrlMapValue;

    // Check if the channel has been requested before, setting status to pending if not
    let status = URLMAP.get_status_or_init(&request.url).await;

    // Begin fetching if necessary, then respond
    let response = match status {
        Some(UrlMapValue::Pending) => AddChannelResp::Ongoing,
        Some(UrlMapValue::Exists(s)) => AddChannelResp::Success { shortname: s },
        Some(UrlMapValue::Unusable(s)) => AddChannelResp::Failure { error: s },
        None => {
            tokio::spawn(async move {
                rssproc::new_channel(request.url).await;
            });
            AddChannelResp::Initiated
        }
    };

    Ok(warp::reply::json(&response))
}
