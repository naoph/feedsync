use reqwest::Client;
use rss::Channel;
use snafu::{ResultExt, Snafu};
use url::Url;

pub struct WebClient {
    client: reqwest::Client,
}

impl WebClient {
    /// Construct a new client with a custom user agent
    pub fn new() -> WebClient {
        let client = Client::builder()
            .user_agent(format!("feedsync-server/{}", env!("CARGO_PKG_VERSION")))
            .build()
            .unwrap();

        WebClient {
            client,
        }
    }

    /// Download and parse a remote rss feed
    pub async fn get_channel(&self, url: Url) -> Result<Channel, GetChannelError> {
        let remote_response = self.client
            .get(url.clone())
            .send()
            .await
            .context(GetResponseSnafu)?;
        let raw_feed = remote_response.bytes()
            .await
            .context(GetBytesSnafu)?;
        let channel = Channel::read_from(&raw_feed[..])
            .context(ParseChannelSnafu)?;

        Ok(channel)
    }
}

#[derive(Debug, Snafu)]
pub enum GetChannelError {
    #[snafu(display("The remote feed could not be loaded: {source}"))]
    GetResponse { source: reqwest::Error },

    #[snafu(display("The remote feed could not be rendered as bytes: {source}"))]
    GetBytes { source: reqwest::Error },

    #[snafu(display("The remote feed could not be parsed: {source}"))]
    ParseChannel { source: rss::Error },
}
