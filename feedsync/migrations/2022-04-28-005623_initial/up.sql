CREATE TABLE channels (
    -- Technical metadata generated locally
    id SERIAL PRIMARY KEY,
    remote text UNIQUE NOT NULL,
    shortname text UNIQUE NOT NULL,
    active bool NOT NULL,

    -- Data specified by the feed
    d_title text NOT NULL,
    d_link text NOT NULL,
    d_description text NOT NULL,
    d_categories text[] NOT NULL,
    d_image text,
    d_author text,
    d_subtitle text,

    -- Cached media
    c_image text
);
