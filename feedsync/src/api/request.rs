use serde::{Deserialize, Serialize};
use url::Url;

/// A request to add a channel
#[derive(Debug, Deserialize, Serialize)]
pub struct AddChannelReq {
    pub url: Url,
}
