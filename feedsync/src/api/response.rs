use serde::{Deserialize, Serialize};

/// Response to requests to add a channel
#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "result")]
#[serde(rename_all(serialize = "lowercase", deserialize = "lowercase"))]
pub enum AddChannelResp {
    /// New and valid URL, addition to be initiated
    Initiated,

    /// Previously requested, still ongoing
    Ongoing,

    /// Previously requested, since succeeded
    Success { shortname: String },

    /// Previously requested, since failed
    Failure { error: String },

    /// Invalid URL
    InvalidUrl,
}

/// Response to request for server version
#[derive(Debug, Deserialize, Serialize)]
pub struct VersionResp {
    version: String,
}

impl From<&str> for VersionResp {
    fn from(version: &str) -> Self {
        VersionResp { version: version.to_string() }
    }
}
