table! {
    channels (id) {
        id -> Int4,
        remote -> Text,
        shortname -> Text,
        active -> Bool,
        d_title -> Text,
        d_link -> Text,
        d_description -> Text,
        d_categories -> Array<Text>,
        d_image -> Nullable<Text>,
        d_author -> Nullable<Text>,
        d_subtitle -> Nullable<Text>,
        c_image -> Nullable<Text>,
    }
}
