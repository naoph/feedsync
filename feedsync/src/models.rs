use serde::{Deserialize, Serialize};

use super::schema::channels;

/// A channel as it exists in the database
#[derive(Debug, Deserialize, Queryable, Serialize)]
pub struct DbChannel {
    // Technical metadata generated locally
    pub id: i32,
    pub remote: String,
    pub shortname: String,
    pub active: bool,

    // Data specified by the feed
    pub d_title: String,
    pub d_link: String,
    pub d_description: String,
    pub d_categories: Vec<String>,
    pub d_image: Option<String>,
    pub d_author: Option<String>,
    pub d_subtitle: Option<String>,

    // Cached media
    pub c_image: Option<String>,
}

/// A channel for insertion to the database
#[derive(Debug, Deserialize, Insertable, Serialize)]
#[table_name = "channels"]
pub struct InsChannel {
    // Technical metadata generated locally
    pub remote: String,
    pub shortname: String,
    pub active: bool,

    // Data specified by the feed
    pub d_title: String,
    pub d_link: String,
    pub d_description: String,
    pub d_categories: Vec<String>,
    pub d_image: Option<String>,
    pub d_author: Option<String>,
    pub d_subtitle: Option<String>,

    // Cached media
    pub c_image: Option<String>,
}
